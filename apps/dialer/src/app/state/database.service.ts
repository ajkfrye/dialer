import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { callers_seed, lists_seed, organizations_seed } from './db-seed';
import { Caller } from './models/agent';

// CANNOT USE RXDB BECAUSE OF https://github.com/pubkey/rxdb/issues/2563
//
// // important that we don't import from the base 'rxdb' package
// import {
//   createRxDatabase,
//   addRxPlugin,
//   RxCollectionCreator,
//   RxDatabase,
// } from 'rxdb/plugins/core';
// import { RxDBNoValidatePlugin } from 'rxdb/plugins/no-validate';
// import { RxDBLeaderElectionPlugin } from 'rxdb/plugins/leader-election';
// import * as PouchdbAdapterMemory from 'pouchdb-adapter-memory';

// import { Logger } from '../logger.service';

// import { listSchema } from './schemas/list.schema';
// import { RxListCollection } from './models/list';
// import { contactSchema } from './schemas/contact.schema';
// import { RxContactCollection, RxContactDocument } from './models/contact';
// import { RxOrganizationCollection } from './models/organization';
// import { organizationSchema } from './schemas/organization.schema';
// import { lists_seed, organizations_seed } from './db-seed';
// import { environment } from '../../environments/environment';

// interface RxDialerCollections {
//   organizations: RxOrganizationCollection;
//   lists: RxListCollection;
//   contacts: RxContactCollection;
// }

// const collectionsInitData: RxCollectionCreator[] = [
//   {
//     name: 'organizations',
//     schema: organizationSchema,
//   },
//   {
//     name: 'lists',
//     schema: listSchema,
//   },
//   {
//     name: 'contacts',
//     schema: contactSchema,
//     methods: {
//       name(this: RxContactDocument) {
//         return `${this.firstName} ${this.lastName}`;
//       },
//     },
//   },
// ];

// /**
//  * creates the database
//  */
// async function createDB(logger: Logger) {
//   addRxPlugin(RxDBLeaderElectionPlugin);
//   addRxPlugin(PouchdbAdapterMemory);

//   /**
//    * to reduce the build-size,
//    * we use some modules in dev-mode only
//    */
//   if (!environment.production) {
//     await Promise.all([
//       // add dev-mode plugin
//       // which does many checks and add full error-messages
//       import('rxdb/plugins/dev-mode').then((m) => addRxPlugin(m)),

//       // we use the schema-validation only in dev-mode
//       // this validates each document if it is matching the jsonschema
//       // import('rxdb/plugins/validate').then((m) => addRxPlugin(m)),
//       import('rxdb/plugins/no-validate').then((m) => addRxPlugin(m)),
//     ]);
//   } else {
//     // in production we use the no-validate module instead of the schema-validation
//     // to reduce the build-size
//     addRxPlugin(RxDBNoValidatePlugin);
//   }

//   logger.debug('DatabaseService: creating database..');

//   const db = await createRxDatabase<RxDialerCollections>({
//     name: 'dialer',
//     adapter: 'memory',
//   });

//   logger.debug('DatabaseService: created database');

//   if (!environment.production) {
//     // show leadership in title
//     db.waitForLeadership().then(() => {
//       logger.info('isLeader now');
//       document.title = '♛ ' + document.title;
//     });
//   }

//   // create collections
//   logger.debug('DatabaseService: create collections');
//   await Promise.all(
//     collectionsInitData.map((colData) => db.collection(colData))
//   );

//   return db;
// }

// let RXDB_INSTANCE: RxDatabase<RxDialerCollections>;

// /**
//  * This is run via APP_INITIALIZER in app.module.ts
//  * to ensure the database exists before the angular-app starts up
//  */
// export async function initRxDatabase(logger: Logger) {
//   logger.debug('initRxDatabase()');

//   RXDB_INSTANCE = await createDB(logger);

//   if (!environment.production) {
//     // we intentionally don't wait for this promise to complete
//     logger.debug('seeding database...');
//     Promise.all([
//       RXDB_INSTANCE.organizations.bulkInsert(organizations_seed),
//       RXDB_INSTANCE.lists.bulkInsert(lists_seed),
//     ]).then(() => logger.debug('seeding database complete'));
//   }
// }

@Injectable({
  providedIn: 'root',
})
export class DatabaseService {
  getLists(args: { organizationId: string }) {
    return of(lists_seed).pipe(
      map((s) => s.filter((d) => d.organizationId === args.organizationId))
    );
  }

  getList(id: string) {
    return of(lists_seed).pipe(map((s) => s.find((d) => d.id === id) ?? null));
  }

  getOrganizations() {
    return of(organizations_seed);
  }

  getOrganization(id: string) {
    return of(organizations_seed).pipe(
      map((s) => s.find((d) => d.id === id) ?? null)
    );
  }

  getCallers(args: { organizationId: string }) {
    return of(callers_seed).pipe(
      map((s) =>
        s
          .filter((d) => d.organizationId === args.organizationId)
          .map((d) => new Caller(d))
      )
    );
  }

  getCaller(id: string) {
    return of(callers_seed).pipe(
      map((s) => s.find((d) => d.id === id) ?? null),
      map((d) => d && new Caller(d))
    );
  }
}
