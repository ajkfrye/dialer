import { RxJsonSchema } from 'rxdb/plugins/core';
import { IList } from '../models/list';

export const listSchema: RxJsonSchema<IList> = {
  title: 'list schema',
  description: 'describes a phone list',
  version: 0,
  keyCompression: false,
  type: 'object',
  properties: {
    id: {
      type: 'string',
      primary: true,
      final: true,
    },
    organizationId: {
      type: 'string',
      final: true,
    },
    name: {
      type: 'string',
    },
  },
  required: ['id', 'organizationId', 'name'],
};
