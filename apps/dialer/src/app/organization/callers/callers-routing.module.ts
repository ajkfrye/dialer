import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CallersComponent } from './callers.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CallersComponent,
  },
  // {
  //   path: ':id',
  //   loadChildren: () => import('./list/list.module').then((m) => m.ListModule),
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CallersRoutingModule {}
