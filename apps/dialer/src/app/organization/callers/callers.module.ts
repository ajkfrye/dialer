import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CallersRoutingModule } from './callers-routing.module';
import { CallersComponent } from './callers.component';


@NgModule({
  declarations: [CallersComponent],
  imports: [
    CommonModule,
    CallersRoutingModule
  ]
})
export class CallersModule { }
