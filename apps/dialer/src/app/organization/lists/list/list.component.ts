import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  template: `
    <!-- <mat-toolbar>
      <mat-toolbar-row> List </mat-toolbar-row>
    </mat-toolbar> -->

    <nav mat-tab-nav-bar>
      <a mat-tab-link routerLink="overview"> Overview </a>
      <a mat-tab-link routerLink="contacts"> Contacts </a>
      <a mat-tab-link routerLink="edit"> Edit </a>
    </nav>

    <router-outlet></router-outlet>
  `,
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
