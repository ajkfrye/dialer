import { TemplatePortal } from '@angular/cdk/portal';
import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { HeaderService } from '../header.service';
import { NavMenuService } from '../nav-menu.service';
import { DatabaseService } from '../state/database.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss'],
})
export class OrganizationComponent implements OnInit {
  @ViewChild('header', { static: true }) header: TemplateRef<unknown>;
  @ViewChild('nav', { static: true }) nav: TemplateRef<unknown>;

  organization = this.databaseService.getOrganization('org-1');

  constructor(
    private headerService: HeaderService,
    private navMenuService: NavMenuService,
    private viewContainerRef: ViewContainerRef,
    private databaseService: DatabaseService
  ) {}

  ngOnInit(): void {
    this.headerService.update(
      new TemplatePortal(this.header, this.viewContainerRef)
    );

    this.navMenuService.update(
      new TemplatePortal(this.nav, this.viewContainerRef)
    );
  }
}
