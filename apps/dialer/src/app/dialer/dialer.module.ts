import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';

import { DialerRoutingModule } from './dialer-routing.module';
import { DialerComponent } from './dialer.component';

@NgModule({
  imports: [CommonModule, DialerRoutingModule, MatIconModule, MatToolbarModule],
  declarations: [DialerComponent],
})
export class DialerModule {}
