export interface ICaller {
  id: string;
  organizationId: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  emailAddress: string;
  online: boolean;
  status: 'READY' | 'DIALING' | 'NOT_READY' | 'IN_CALL';
}

export class Caller {
  id: string;
  organizationId: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  emailAddress: string;
  online: boolean;
  status: 'READY' | 'DIALING' | 'NOT_READY' | 'IN_CALL';

  constructor(args: ICaller) {
    Object.assign(this, args);
  }

  get name() {
    return `${this.firstName} ${this.lastName}`;
  }
}
