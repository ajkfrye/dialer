// import { RxCollection, RxDocument } from 'rxdb/plugins/core';

export interface IContact {
  id: string;
  listId: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
}

// interface IContactMethods {
//   name(): string;
// }

// export type RxContactDocument = RxDocument<IContact, IContactMethods>;
// export type RxContactCollection = RxCollection<IContact, IContactMethods>;

// export class Contact {
//   id: string;
//   listId: string;
//   firstName: string;
//   lastName: string;
//   phoneNumber: string;

//   constructor(args: IContact) {
//     Object.assign(this, args);
//   }

//   name() {
//     return `${this.firstName} ${this.lastName}`;
//   }
// }
