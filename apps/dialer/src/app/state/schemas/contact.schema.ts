import { RxJsonSchema } from 'rxdb/plugins/core';
import { IContact } from '../models/contact';

// interface IContactMethods {
//   name(): string;
// }

// export type RxHeroDocument = RxDocument<IContact, IContactMethods>;

export const contactSchema: RxJsonSchema<IContact> = {
  title: 'contact schema',
  description: 'describes a contact',
  version: 0,
  keyCompression: false,
  type: 'object',
  properties: {
    id: {
      type: 'string',
      primary: true,
      final: true,
    },
    listId: {
      type: 'string',
      final: true,
    },
    firstName: {
      type: 'string',
    },
    lastName: {
      type: 'string',
    },
    phoneNumber: {
      type: 'string',
    },
  },
  required: ['id', 'listId', 'firstName', 'lastName', 'phoneNumber'],
};

// const schema: RxJsonSchema = {
//   title: 'lists schema',
//   description: 'describes a phone list',
//   version: 0,
//   keyCompression: false,
//   type: 'object',
//   properties: {
//     name: {
//       type: 'string',
//       primary: true,
//     },
//     color: {
//       type: 'string',
//       default: '',
//       minLength: 3,
//     },
//     maxHP: {
//       type: 'number',
//       minimum: 0,
//       maximum: 1000,
//     },
//     hp: {
//       type: 'number',
//       minimum: 0,
//       maximum: 100,
//       default: 100,
//     },
//     team: {
//       description: 'color of the team this hero belongs to',
//       type: 'string',
//     },
//     skills: {
//       type: 'array',
//       maxItems: 5,
//       uniqueItems: true,
//       items: {
//         type: 'object',
//         properties: {
//           name: {
//             type: 'string',
//           },
//           damage: {
//             type: 'number',
//           },
//         },
//       },
//       default: [],
//     },
//   },
//   required: ['name'],
// };
