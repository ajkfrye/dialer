import { TemplatePortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class NavMenuService {
  private _component$ = new ReplaySubject<TemplatePortal<unknown>>(1);

  // the delay is necessary to avoid changed after checked errors
  component$ = this._component$.pipe(delay(0));

  update(template: TemplatePortal<unknown>) {
    this._component$.next(template);
  }
}
