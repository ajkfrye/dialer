import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { ContactsComponent } from './contacts/contacts.component';
import { OverviewComponent } from './overview/overview.component';
import { EditComponent } from './edit/edit.component';

@NgModule({
  imports: [
    CommonModule,
    ListRoutingModule,
    MatIconModule,
    MatTabsModule,
    MatToolbarModule,
  ],
  declarations: [ListComponent, ContactsComponent, OverviewComponent, EditComponent],
})
export class ListModule {}
