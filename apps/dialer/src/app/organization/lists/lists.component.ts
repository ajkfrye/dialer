import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DatabaseService } from '../../state/database.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss'],
})
export class ListsComponent implements OnInit {
  lists = this.databaseService.getLists({ organizationId: 'org-1' });

  constructor(
    private dialog: MatDialog,
    private databaseService: DatabaseService
  ) {}

  ngOnInit(): void {}

  async addList() {
    const module = await import('./add-list/add-list.module');

    this.dialog.open(module.AddListModule.component, {
      width: '600px',
    });
  }
}
